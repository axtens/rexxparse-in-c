A port of Oscar Brain's Rexx Parse in VB6 to C#.

**Changes**

*2019-12-10* 

- Fixed issue with missing AssemblyInfo.cs
- Added default case to switch statements
- Added System.Linq using ahead of Linq-ification (pending)

MIT license.

