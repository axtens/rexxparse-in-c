﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;


public class Rexx
{
    //BMA: error checking and trapping is not complete. it is possible to crash this just by giving (9) in the pattern.

    //   Collection

    // ********************************************************************************
    // * Description:
    // *             This module implements the statement PARSE of the
    // *             Command Language REXX
    // * Comments:
    // *             PARSE is maybe the best implementation of a parsing
    // *             algorithm, but to master it is not easy as it has endless
    // *             possibilities.
    // *             The full implementation include a precompiler that allows
    // *             to declare the statement exactly as in REXX; the precompiler
    // *             then analyzes the statement an it expands in to call
    // *             the PARSE routine.
    // *  This is an implementation of a Parsing Algorithm that recognize the following patterns
    // *  as Anchors:
    // *   1.  Number      (for absolute position)
    // *   2.  +           (for relative position)
    // *   3.  -           (for moving back)
    // *   4.  value       (for controled position)
    // *   5.  literals    (for fixed anchors)
    // *   6.  ignore word (for undesirable substrings)
    // *   7.  word        (to extract the next word in the string)
    // *  Because the lack of an "INTERPRET" statement in VB,
    // *   there is a need to create a system pool of variables.
    // *
    // * Examples:
    // *  The string being any VB string can be parsed by absolute position
    // *  like in:
    // *  instring="This is a test for Parse"
    // *  Pattern = "2 var1 10 15 var2"
    // *       Result Var1 = "his is a"
    // *              Var2 = " for Parse"
    // *  Can also be parse with relative position like in:
    // *  Pattern = "2 var1 +8 +2 var2"
    // *       Result Var1 = "his is a"
    // *              Var2 = "est for Parse"
    // *  Can also be parsed with extracted values like in:
    // *  instring = "08This is a test for Parse"
    // *  Pattern = var1 2 var2 +(var1) var3 var4
    // *       Result Var1 = "08"
    // *              Var2 = "This is "
    // *              Var3 = "a "
    // *              Var4 = "test for Parse"
    // *  Can also Parse moving back, extracting to the end
    // *  instring = "This is a test for Parse"
    // *  Pattern = "var1 10 var2 -3 var3 -5 var4"
    // *       Result Var1 = "This is a "
    // *              Var2 = " a test for Parse"
    // *              Var3 = "Parse"
    // *              Var4= vbnullstring
    // *  Can also Parse anchoring on quoted literals
    // *  instring = "This is a test for Parse"
    // *  Pattern = "var1 "is" var2 "of" var3"
    // *       Result Var1 = "Th"
    // *              Var2 = " is a test "
    // *              Var3 = " Parse"
    // *
    // *
    // *  The extraction is performed after fixing two anchors, the AnchorA is the left position to
    // *     extract from and the AnchorE is the right position to extract up to.
    // *  The anchors are the following
    // *  1. A number indicates absolute position and it can be represented by:
    // *        <number>|(VarName)
    // *  2. A plus sign indicates a relative position to add to anchor A
    // *        +<number>|+(VarName)
    // *  3. A Minus sign to extract the n right characters.
    // *        -<number>|-(VarName)
    // *  4. An Equal Sign indicates length of the extracted value, similar to +
    // *        =<number>|=(VarName)
    // *  5. A quoted literal where the quote could be single|double, it is case sensitive, fixes the
    // *     anchorE for the left variable and the anchorA for the right variable
    // *       "<any literal>"|'<any Literal>'
    // *  6. a Period allows to jump (ignore) over a word.
    // *  7. a VarName with no explicit anchors refers to a full word extraction.
    // *
    // *  Usage:
    // *     1. If a variable value will be used in the extraction then it
    // *        should be set into the pool.
    // *        EnterSVPool "MyVar",MyVar
    // *        With this we are creating a Pool Variable named MyVar that will
    // *        take the value of VB variable MyVar.  Observe that the pool
    // *        variable does not have to be named as the VB variable; we could
    // *        create a Pool variable like:
    // *        EnterSVPool "EmployeeId",EmployeeCode
    // *
    // *     2. The text to parse is any text it can be passed as is or through a
    // *        variable.
    // *        Instring = "<whatever>"
    // *     3. The pattern defines the way to parse
    // *        Pattern = "var1 +3 LastName 12 FirstName ."
    // *        In this case:
    // *        - The first word to be assigned to the pool variable
    // *          Var1 (if it does not exist, it will be created)
    // *        - Skip 3 characters from the end of previous extraction.
    // *        - Next extraction to be up to position 12 of the original
    // *          string to be assigned to the Pool Variable LastName
    // *        - Next word to be assigned to Pool Variable FirstName
    // *        - The rest of the string to be ignored (.)
    // *      4. Calling the PARSE instruction
    // *         PARSE Instring,Pattern
    // *      5. Retrieving the result
    // *         MyVar1 = RetrieveSVPool("Var1")
    // *         LastName = RetrieveSVPool("LastName")
    // *         NickName = RetrieveSVPool("FIRSTNAME")
    // *         FirstName = RetrieveSVPool("firstname")
    // *      Observe that the Variable VB names do not have to be equal to
    // *      the Pool names; the Pool variable name are not case sensitive;
    // *      and, the persistance of the values.
    // *      The values are kept in the pool until the pool is clear, no need
    // *      to clear the pool
    // *
    // *      Full implemetation consist of a precompiler to expand certain macros
    // *      One of them being Parse. PARSE then will be written as:
    // *
    // *     PARSE linein, LastName s_comma FirstName Others
    // *
    // *      The precompiler will recognize the Macro an expand it into:
    // *     '*  PARSE Linein LastName s_comma FirstName Others
    // *     '*exec Parse
    // *       PARSE Linein, LastName s_comma FirstName Others
    // *       LastName = RetrieveSVPool("LastName")
    // *       NickName = RetrieveSVPool("FIRSTNAME")
    // *       linein = RetrieveSVPool("Others")
    // *     '*end exec Parse
    // *
    // * Revisions:
    // *  4/12/2004  obc Added comments.
    // *
    // *
    // ********************************************************************************
    Parsing_Pattern[] MPattern = new Parsing_Pattern[21];
    string Sep;
    int iPos;
    //int AnchorA;
    //int AnchorE;
    public Dictionary<string, string> dict = new Dictionary<string, string>();

    //bool NextVar;
    public int PARSE(string InString, string pattern)
    {
        //********************************************************************************
        //* Name: PARSE
        //*
        //* Description:     It manages the basic functions of the implementation
        //*                  1. Parsing of the Pattern into a comprehensible set
        //*                     of control variables for further parsing
        //*                  2. Actual parsing of the input string based on the
        //*                     control string
        //* Parameters:       InString           '  text to be parsed
        //*                    Pattern           '  Parsing pattern
        //* Created: 4/12/2004 10:38:35 AM  Oscar Brain
        //********************************************************************************
        int Parse_ierr = 0;
        ParsePattern(pattern, ref Parse_ierr);
        ParseText(InString, ref Parse_ierr);
        return Parse_ierr;
    }

    private void ParsePattern(string pattern, ref int Parse_ierr, bool bDontCLear = false)
    {
        //********************************************************************************
        //* Name: ParsePattern
        //*
        //* Description:     This routine analyzes the pattern to create a control
        //*                  array to Parse the text
        //* Parameters:       Pattern
        //*                    Parse_ierr
        //* Created: 3/26/2004 2:45:07 PM  Oscar Brain
        //********************************************************************************
        string MySymb = null;
        string A = null;
        int i = 0;
        int j = 0;
        if (!bDontCLear)
        {
            dict.Clear();
        }
        for (i = 0; i <= 20; i++)
        {
            MPattern[i].PType = "";
            MPattern[i].PValue = "";
            MPattern[i].RValue = "";
        }
        i = 0;
        pattern = Strings.Trim(pattern);
        while (pattern != "")
        {
            i = i + 1;
            switch (Strings.Left(pattern, 1))
            {
                case ".":
                    MPattern[i].PType = "P";
                    pattern = Strings.Trim(Strings.Mid(pattern + Strings.Space(1), 2));
                    break;
                case "+":
                case "-":
                    MPattern[i].PType = Strings.Left(pattern, 1);
                    pattern = Strings.Trim(Strings.Mid(pattern + Strings.Space(1), 2));
                    if (Strings.Left(pattern, 1) == "(")
                    {
                        j = Strings.InStr(pattern, ")");
                        MPattern[i].PValue = Strings.Left(pattern, j);
                        pattern = Strings.Mid(pattern, j + 1);
                    }
                    else
                    {
                        MPattern[i].PValue = ExtractNumber(ref pattern).ToString();
                    }
                    pattern = Strings.Trim(pattern);
                    break;
                case "(":
                    j = Strings.InStr(2, pattern, ")");
                    if (j == 0)
                    {
                        Parse_ierr = 1;
                        return;
                    }
                    MySymb = Strings.Mid(pattern, 2, j - 2);
                    pattern = Strings.Trim(Strings.Mid(pattern, j + 1));
                    A = dict[MySymb];
                    Debug.Print("retrievesvPuddle of " + MySymb + " gives " + A);
                    MPattern[i].PType = "A";
                    MPattern[i].PValue = "(" + MySymb + ")";
                    break;
                case "\"":
                case "'":
                    MPattern[i].PType = "Q";
                    j = Strings.InStr(2, pattern, Strings.Left(pattern, 1));
                    if (j == 0)
                    {
                        Parse_ierr = 2;
                        return;
                    }
                    MPattern[i].PValue = Strings.Left(pattern, j);
                    pattern = Strings.Trim(Strings.Mid(pattern + Strings.Space(1), j + 1));
                    break;
                case "A":
                case "B":
                case "C":
                case "D":
                case "E":
                case "F":
                case "G":
                case "H":
                case "I":
                case "J":
                case "K":
                case "L":
                case "M":
                case "N":
                case "O":
                case "P":
                case "Q":
                case "R":
                case "S":
                case "T":
                case "U":
                case "V":
                case "W":
                case "X":
                case "Y":
                case "Z":

                case "a":
                case "b":
                case "c":
                case "d":
                case "e":
                case "f":
                case "g":
                case "h":
                case "i":
                case "j":
                case "k":
                case "l":
                case "m":
                case "n":
                case "o":
                case "p":
                case "q":
                case "r":
                case "s":
                case "t":
                case "u":
                case "v":
                case "w":
                case "x":
                case "y":
                case "z":

                case "_":
                    // we use the __ etc as paramters for parse construct
                    MPattern[i].PType = "V";
                    MPattern[i].PValue = GetToken(ref pattern, " ", ref Sep);
                    //UCase()
                    break;
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "8":
                case "7":
                case "9":
                    MPattern[i].PType = "A";
                    MPattern[i].PValue = GetToken(ref pattern, " ", ref Sep);
                    break;
                default:
                    break;
            }
        }
        MPattern[i + 1].PType = "E";
    }
    private void ParseText(string InString, ref int Parse_ierr)
    {
        //********************************************************************************
        //* Name: ParseText
        //*
        //* Description:     Based on the control array (sequence of controls)
        //*                  this procedure extracts the substrings and
        //*                  creates the symbolic variable entries with their
        //*                  associated values
        //* Parameters:       InString
        //*                    Parse_ierr
        //* Created: 3/26/2004 2:46:49 PM  Oscar Brain
        //********************************************************************************
        int i = 0;
        int j = 0;
        int AddNext = 0;
        int Addi = 0;
        string Qstr = null;
        int AnchorA = 0;
        int AnchorE = 0;
        bool NextVar = false;
        bool SkipNext = false;
        bool SkipComp = false;
        i = 0;
        AnchorA = 0;
        NextVar = false;
        Addi = 0;
        while (true)
        {
            SkipNext = false;
            i = i + 1 + Addi;
            Addi = 0;
            if (MPattern[i].PType == "")
                break; // TODO: might not be correct. Was : Exit Do
            switch (MPattern[i].PType)
            {
                case "A":
                    if (NextVar == true)
                        Parse_ierr = 3;
                    AnchorA = Convert.ToInt32(Strings.Trim(MPattern[i].PValue));
                    NextVar = true;
                    SkipNext = true;
                    break;
                case "V":
                    NextVar = false;
                    if (AnchorA == 0)
                        AnchorA = 1;
                    if (SkipComp)
                    {
                        SkipNext = true;
                        MPattern[i].RValue = Strings.Mid(InString, AnchorA, AnchorE - AnchorA + 1);
                    }
                    break;
                case "P":
                    if (NextVar == true)
                        Parse_ierr = 3;
                    iPos = Strings.InStr(AnchorA + 1, InString, " ");
                    if (iPos == 0)
                    {
                        AnchorA = Strings.Len(InString);
                        //move to next nonspace
                    }
                    else
                    {
                        while (Strings.InStr(iPos, InString, " ") == iPos)
                        {
                            iPos = iPos + 1;
                        }
                        AnchorA = iPos;
                    }
                    //AnchorA = IIf(iPos = 0, Len(InString), iPos)
                    SkipNext = true;
                    break;
                case "Q":
                    if (NextVar == true)
                        Parse_ierr = 3;
                    Qstr = Strings.Trim(MPattern[i].PValue);
                    iPos = Strings.InStr(AnchorA + 1, InString, Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2));
                    AnchorA = (iPos == 0 ? Strings.Len(InString) : iPos + Strings.Len(Qstr) - 2);
                    i = i + 1;
                    //    &&&
                    break;
                case "+":
                    if (Strings.Left(MPattern[i].PValue, 1) == "(")
                    {
                        Qstr = Strings.Trim(MPattern[i].PValue);
                        Qstr = Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2);
                        MPattern[j].PValue = dict[Qstr];
                    }
                    AnchorA = AnchorA + Convert.ToInt32(Strings.Trim(MPattern[i].PValue)) - 1;
                    NextVar = true;
                    SkipNext = true;
                    break;
                case "-":
                    if (Strings.Left(MPattern[i].PValue, 1) == "(")
                    {
                        Qstr = Strings.Trim(MPattern[i].PValue);
                        Qstr = Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2);
                        MPattern[j].PValue = dict[Qstr];
                    }
                    AnchorA = AnchorA - Convert.ToInt32(Strings.Trim(MPattern[i].PValue));
                    AnchorE = Strings.Len(InString);
                    NextVar = true;
                    SkipNext = true;
                    SkipComp = true;
                    break;
                default:
                    break;
            }
            bool vee = false;
            if (!SkipNext)
            {
                j = i + 1;
                switch (MPattern[j].PType)
                {
                    case "":
                    case "E":
                        AnchorE = Strings.Len(InString);
                        break;
                    case "P":
                        vee = true;
                        iPos = Strings.InStr(AnchorA + 1, InString, " ");
                        if (iPos == 0)
                        {
                            AnchorE = Strings.Len(InString);
                        }
                        else
                        {
                            while (Strings.InStr(iPos, InString, " ") == iPos)
                            {
                                iPos = iPos + 1;
                            }
                            AnchorE = iPos - 1;
                        }
                        break;
                    case "-":
                        if (Strings.Left(MPattern[j].PValue, 1) == "(")
                        {
                            Qstr = Strings.Trim(MPattern[j].PValue);
                            Qstr = Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2);
                            MPattern[j].PValue = dict[Qstr];
                        }
                        AnchorE = Strings.Len(InString);
                        AnchorA = AnchorA - Convert.ToInt32(MPattern[j].PValue);
                        Addi = 1;
                        SkipNext = false;
                        AddNext = 0;
                        break;
                    case "A":
                        AnchorE = Convert.ToInt32(Strings.Trim(MPattern[j].PValue));
                        Addi = 1;
                        break;
                    case "+":
                        if (Strings.Left(MPattern[j].PValue, 1) == "(")
                        {
                            Qstr = Strings.Trim(MPattern[j].PValue);
                            Qstr = Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2);
                            MPattern[j].PValue = dict[Qstr];
                        }
                        AnchorE = AnchorA + Convert.ToInt32(Conversion.Val(Strings.Trim(MPattern[j].PValue)));
                        Addi = 1;
                        break;
                    case "V":
                        vee = true;
                        iPos = Strings.InStr(AnchorA + 1, InString, " ");
                        if (iPos == 0)
                        {
                            AnchorE = Strings.Len(InString);
                        }
                        else
                        {
                            while (Strings.InStr(iPos, InString, " ") == iPos)
                            {
                                iPos = iPos + 1;
                            }
                            AnchorE = iPos - 1;
                        }
                        break;
                    case "Q":
                        Qstr = Strings.Trim(MPattern[j].PValue);
                        Qstr = Strings.Mid(Qstr, 2, Strings.Len(Qstr) - 2);
                        AddNext = Strings.Len(Qstr);
                        iPos = Strings.InStr(AnchorA + 1, InString, Qstr);
                        AnchorE = (iPos == 0 ? Strings.Len(InString) : iPos - 1);
                        Addi = 1;
                        break;
                    default:
                        break;
                }
                //      If AnchorA = 0 Then
                //        EnterSVPuddle MPattern(i).PValue, ""
                //      Else
                //        EnterSVPuddle MPattern(i).PValue, Mid(InString, AnchorA, AnchorE - AnchorA + 1) & chr$(167)
                //      End If
                string sKey = "";
                string sVal = "";
                if (AnchorA == 0 | AnchorA > Strings.Len(InString))
                {
                    sKey = Strings.Trim(MPattern[i].PValue);
                    if (sKey != "")
                    {
                        dict[sKey] = "";
                    }
                }
                else
                {
                    sKey = Strings.Trim(MPattern[i].PValue);
                    int iLength = AnchorE - AnchorA + 1;
                    sVal = Strings.Mid(InString, AnchorA, (iLength < 0 ? Strings.Len(InString) : iLength));
                    if (!string.IsNullOrEmpty(sKey))
                    {
                        dict[sKey] = (vee ? sVal.Trim() : sVal);
                    }
                }

                if (!SkipNext)
                    AnchorA = AnchorE + 1 + AddNext;
                AddNext = 0;
            }
        }
    }

    public int ExtractNumber(ref string sLine)
    {
        int functionReturnValue = 0;
        //********************************************************************************
        //* Name: ExtractNumber
        //*
        //* Description:     Given a text parameter it extracts the first
        //*                  consecutive characters that makes up a number, if any
        //* Parameters:       sLine
        //* Created: 3/26/2004 2:48:44 PM  Oscar Brain
        //********************************************************************************
        int Char1 = 0;
        sLine = Strings.Trim(sLine);
        functionReturnValue = 0;
        if (sLine == "")
            return functionReturnValue;
        Char1 = Strings.Asc(sLine);
        while (Char1 > 47 & Char1 < 58)
        {
            sLine = Strings.Mid(sLine, 2);
            functionReturnValue = functionReturnValue * 10 + Char1 - 48;
            if (sLine == "")
                break; // TODO: might not be correct. Was : Exit Do
            Char1 = Strings.Asc(sLine);
        }
        return functionReturnValue;
    }
    public bool IsNumber(string texto)
    {
        bool functionReturnValue = false;
        //********************************************************************************
        //* Name: IsNumber
        //*
        //* Description:      It validates a text as a number
        //* Parameters:       texto
        //* Created: 3/26/2004 2:50:33 PM  Oscar Brain
        //********************************************************************************
        string sData = null;
        int i = 0;
        texto = Strings.Trim(texto);
        if (texto == "")
        {
            functionReturnValue = false;
            return functionReturnValue;
        }
        functionReturnValue = true;
        for (i = 1; i <= Strings.Len(texto); i++)
        {
            sData = Strings.Mid(texto, i, 1);
            if (Strings.Asc(sData) < 48 | Strings.Asc(sData) > 57)
            {
                functionReturnValue = false;
                break; // TODO: might not be correct. Was : Exit For
            }
        }
        return functionReturnValue;
    }
    public string GetToken(ref string sString, string sSeps, ref string sSep)
    {
        string functionReturnValue = null;
        //********************************************************************************
        //* Name: GetToken
        //*
        //* Description:  This function returns the first token in
        //*         an string where the separator could be any of
        //*         the characters on sSeps.
        //*               It returns:
        //*                 the token
        //*               It sets:
        //*                 the string, by substracting the token
        //*                 the actual separator
        //* Parameters:
        //* Created: 02/25/2003 8:03:35 AM  Oscar Brain
        //********************************************************************************
        int i = 0;
        int iMin = 0;
        int j = 0;
        int k = 0;
        sString = sString + Strings.Space(1);
        k = Strings.Len(sString);
        iMin = k;
        for (i = 1; i <= Strings.Len(sSeps); i++)
        {
            j = Strings.InStr(sString, Strings.Mid(sSeps, i, 1));
            if (j == 0)
                j = k;
            if (j < iMin)
            {
                iMin = j;
                sSep = Strings.Mid(sSeps, i, 1);
            }
        }
        functionReturnValue = Strings.Left(sString, iMin - 1);
        sString = Strings.Trim(Strings.Mid(sString, iMin + 1));
        return functionReturnValue;
    }

}
